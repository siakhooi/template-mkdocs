# Template MkDocs

- GitLab Pages Templates with MkDocs

## Other Alternative

- <https://gitlab.com/pages/mkdocs>

## To run locally

- Requirements
  - Python
  - MkDocs

## Run locally

```bash
mkdocs serve
```

## Build locally

```bash
mkdocs build
```

## Reference

- <https://www.mkdocs.org>
